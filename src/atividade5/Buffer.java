/**
 * Buffer
 * 
 * Autor: Paulo Bordignon
 * Ultima modificacao: 14/04/2018
 */
package atividade5;

public interface Buffer {

    public void set (int value )throws InterruptedException;
    
    public int get () throws InterruptedException;
    
}
