/*
 * Exemplo Produtor/Consumidor com Múltiplas Threads Consumidor
 *
 * Autor: Paulo Bordignon
 * Ultima modificacao: 14/04/2018
 */
package atividade5;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Principal {

    public Principal(){
        
        ExecutorService pool = Executors.newCachedThreadPool();
    
        int numThreads = 1;
        Buffer [] buffer = new BufferSincronizado[numThreads];
        for(int i=0;i<buffer.length;i++){
            buffer[i] = new BufferSincronizado();
        }                
                
        for(int i=0;i<buffer.length;i++){
            pool.execute( new Consumidor( buffer, i ));            
        }
        
        pool.execute( new Produtor (buffer) );
               
    
        pool.shutdown();
    
    }
    
    public static void main(String [] args){
        new Principal();
    }
}
